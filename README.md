# TP1-MABS - Pellegri Tayna ; Vandamme Lea

## Travail réalisé 

Toute la partie obligatoire est fonctionnelle. L'utilisation du code IUPAC est également possible.

Toutes les fonctions ont leur docstring, et des doctests ont été faits pour toutes les fonctions (hors getters/setters/fonctions d'affichage bien entendu).

## Dossiers du projet

- dossier Fichiers : comprend tous nos fichiers de tests : ceux utilisés pour les doctests  (dossier test) mais aussi les fichiers utilisés pour tester le fonctionnement avec le code IUPAC.

- dossier fichiers_test : dossier que vous nous avez fourni, avec les 5 jeux de test.

- les fichiers de code :

1. **Sequence.py** : permet de lire une séquence depuis un fichier fasta, et de créer un objet Sequence comprenant le nom du fichier, le nom de la séquence et la séquence elle même en attribut.

2. **Score.py** : permet de lire un système de scores depuis un fichier txt, et de créer un objet Score comprenant le nom du fichier, et les scores d'identité, substitution et indel en attribut.

3. **Cost.py** : permet de comparer deux nucléotides et de donner le score correspondant. Un objet Cost comprend en attribut le système de score de l'alignement et l'alphabet qu'il accepte.

4. **Alignement.py** : C'est dans cette classe que toutes les fonctions permettant l'alignement sont mises. Un objet alignement prend en attribut les deux séquences à aligner ainsi qu'un objet Cost. 

5. **Main.py** : Pour lancer le tout.

## Lancement du code

La commande est de la forme : 

`$ python3 Main.py <fichier_seq_1> <fichier_seq_1> <fichier_score>
`

- Pour lancer l'alignement avec les fichiers tests fournis :

`python3 Main.py fichiers_test/test<numero_test>/seq1.fasta fichiers_test/test1/seq2.fasta fichiers_test/test<numero_test>/score.txt
`

- Pour lancer l'alignement avec deux séquences utilisant le code IUPAC : 

`python3 Main.py fichiers/seq1.fasta fichiers/seq2.fasta fichiers/score.txt
`

## Lancement des doctests

Il faut lancer les quatres fichiers un par un : 

` python3 Score.py && python3 Sequence.py && python3 Cost.py && python3 Alignment.py

