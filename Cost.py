from Score import *
class ValueError(Exception):
    """
    Exception used by methods

    * ``__call__``
    
    of class :class:`Cost`.
    """
    def __init__(self, msg):
        self.message = msg

class Cost:
    """
    Class Cost - Cost of the alignment of 2 nucleotides
    syst_score : The score system applied 
    carac : the set of authorized characters
    """

    __IUPAC= {'A' : ['A'], 'T' : ['T'],'C' : ['C'],'G' : ['G'],'R' : ['A', 'G'], 'Y': ['C', 'T'], 'S' : ['G', 'C'], 'W' : ['A', 'T'], 'K' : ['G', 'T'],\
    'M' : ['A', 'C'], 'B' : ['C', 'G', 'T'], 'D' : ['A', 'G', 'T'], 'H' : ['A', 'C', 'T'], 'V' : ['A', 'C', 'G'], 'N' : ['C', 'G', 'T', 'A']}


    def __init__(self, syst_score, carac):

        self.__syst_score = syst_score
        self.__carac = carac

    def get_sys_score(self):
        return self.__syst_score

    def get_carac(self):
        return self.__carac

    def __iupac(self, carac1, carac2):
        """
        Compare 2 caracters of the iupac code and return the corresponding score

        Parameters
        ----------
        carac1 : str
        The 1st caracter
        carac2 : str
        The 2nd caracter

        The 2 caracters must be in the iupac code.

        Returns
        -------
        int or ValueError
        The score corresponding to the alignment of the 2 caracters.

            >>> cost = Cost(Score('fichiers/score.txt',2,-1,-1), 'ATCGRBY');
            >>> cost._Cost__iupac('R','Y')
            -1
            >>> cost._Cost__iupac('R','B')
            0
            >>> cost._Cost__iupac('Z','B')
            Traceback (most recent call last):
            ...
            ValueError: Les caractères donnés ne font pas partie du code IUPAC. Vérifiez leur validité.
        """
        if carac1 not in self.__IUPAC.keys() or carac2 not in self.__IUPAC.keys() :
            raise ValueError("Les caractères donnés ne font pas partie du code IUPAC. Vérifiez leur validité.")
        else:
            intersection = False
            nucl1 = self.__IUPAC[carac1]
            nucl2 = self.__IUPAC[carac2]
            for elt in nucl1:
                if elt in nucl2:
                    intersection = True
            if intersection: 
                return 0
            else:
                return self.get_sys_score().get_substitution()

    def __compare(self, carac1, carac2):
        """
        Compare 2 nucleotides (or gap) and give the corresponding score

        Parameters
        ----------
        carac1 : str
        The 1st caracter
        carac2 : str
        The 2nd caracter

        Returns
        -------
        int
        The score corresponding to the alignment of the 2 caracters

            >>> cost = Cost(Score('fichiers/score.txt',2,-1,-1), 'ATCG');
            >>> cost._Cost__compare('A','A')
            2
            >>> cost._Cost__compare('A','C')
            -1
            >>> cost._Cost__compare('-','C')
            -1
            >>> cost._Cost__compare('R','Y')
            -1
            >>> cost._Cost__compare('R','B')
            0
        """
        if carac1 in ['A','T','C','G'] and carac2 in ['A','T','C','G']:
            if carac1 == carac2 :
                return self.get_sys_score().get_identity()
            else:
                return self.get_sys_score().get_substitution()
        elif carac1 == '-' or carac2 == '-':
            return self.get_sys_score().get_indel()
        else:
            return self.__iupac(carac1, carac2)

    def __call__(self, carac1, carac2):
        """
        Validate if the 2 caracters are valid, and compare them to give the corresponding score

        Parameters
        ----------
        carac1 : str
        The 1st caracter
        carac2 : str
        The 2nd caracter

        Returns
        -------
        int or ValueError
        The score corresponding to the alignment of the 2 caracters if they are valid, ValueError otherwise

            >>> cost = Cost(Score('fichiers/score.txt', +2,-1,-1), 'ATCGRYB');
            >>> cost('A','A')
            2
            >>> cost('A','C')
            -1
            >>> cost('-','C')
            -1
            >>> cost('R','Y')
            -1
            >>> cost('R','B')
            0
            >>> cost('Z','A')
            Traceback (most recent call last):
            ...
            ValueError: Les caractères donnés ne peuvent être comparés, vérifiez leur validité.
        """
        if (carac1 not in self.__carac or carac2 not in self.__carac) and (carac1 != '-' and carac2 != '-'):
            raise ValueError("Les caractères donnés ne peuvent être comparés, vérifiez leur validité.")
        elif carac1 in ['-'] and carac2 in ['-']:
            raise ValueError("Deux gaps ne peuvent être comparés.")
        else:
            return self.__compare(carac1, carac2)


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)



