import sys, Cost, Score, Alignment
from Cost import *
from Score import *
from Alignment import *

if __name__ == "__main__":

    seq1 = Sequence(sys.argv[1]) ; seq1.parseFasta()
    seq2 = Sequence(sys.argv[2]) ; seq2.parseFasta()
    systscore = Score(sys.argv[3]) ; systscore.parseConfig()
    
    cost = Cost(systscore , 'ATCGRYSWKMBDHVN')
    
    alignement = Alignment(seq1,seq2,cost)
    alignement(seq1,seq2,cost,10,200)

    