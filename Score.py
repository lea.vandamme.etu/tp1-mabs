from Sequence import *

class ScoreError(Exception):
    """
    Exception used by methods

    * ``__init__``
    * ``head``
    * ``tail``
    
    of class :class:`List`.
    """
    def __init__(self, msg):
        self.message = msg

class Score:

    """
    Class Score - Represent a system of score for an alignment
    fich : The file where the system is read
    identity : the score for an identity between two nucleotides
    substitution : the score for a substitution between two nucleotides
    indel : the score for an indel

    The file read and the scores have to be well formatted : it is verified in the isScore and parseConfig methods.
    """

    def __init__(self, fich, identity = 0, substitution = 0, indel = 0):
        self.__fich = fich
        self.__identity = identity
        self.__substitution = substitution
        self.__indel = indel

    def get_identity(self):
        return self.__identity

    def get_substitution(self):
        return self.__substitution

    def get_indel(self):
        return self.__indel

    def get_fich(self):
        return self.__fich

    def getIupac(self):
        return self.__iupac

    def set_substitution(self, sub):
        self.__substitution = sub

    def set_indel(self, indel):
        self.__indel = indel

    def set_identity(self, identity):
        self.__identity = identity

    def isScore(self):
        """
        Open a score file and check if it is well formated

        Parameters
        ----------

        Returns
        -------

        tuple
        None or False (well formated or not) and the text of the error

        The first line of the file must be formatted like that : #[space]identity substitution indel
        The second line must contain 3 scores, preceded by + or -, separed by a space

            >>> score = Score('fichiers/score.txt');
            >>> score.isScore()
            >>> score = Score('fichiers/tests/scoreMissCroisillonCateg.txt');
            >>> score.isScore()
            (False, "Le croisillon ou l'espace sont manquants sur la première ligne : #[space]identity substitution indel --- Les catégories de score ne sont pas dans le bon ordre ou mal écrits: l'ordre correct est : identity ; substitution ; indel --- ")
            >>> score = Score('fichiers/tests/scorePbScores.txt');
            >>> score.isScore()
            (False, "Le nombre de scores entrés n'est pas correct, il en faut 3 --- Les scores ne sont pas bien formatés, ils doivent commencer par - ou + --- ")
        """
        res = ''
        CROISILLON = "Le croisillon ou l'espace sont manquants sur la première ligne : #[space]identity substitution indel --- "
        CATEG = "Les catégories de score ne sont pas dans le bon ordre ou mal écrits: l'ordre correct est : identity ; substitution ; indel --- "
        NBSCORES = "Le nombre de scores entrés n'est pas correct, il en faut 3 --- "
        FORMATSCORE = "Les scores ne sont pas bien formatés, ils doivent commencer par - ou + --- "
        with open(self.get_fich(), "r") as fich:
            l = fich.readline()
            if l[:2] != '# ':
                res += CROISILLON
            categ = l[2:].rstrip().split()
            if categ[0] != 'identity' or categ[1] != 'substitution' or categ[2] != 'indel' :
                res += CATEG
            l2 = fich.readline()
            scores = l2.rstrip().split()
            if len(scores) != 3:
                res += NBSCORES
            for elt in scores:
                if elt[0] not in ['-', '+']:
                    res += FORMATSCORE
            if res != '':
                return False, res


    def parseConfig(self):
        """
        Parse the score file to complete the Score object 

        Parameters
        ----------

        Returns
        -------

        Nothing
        The attributes identity, substitution and indel are scpecified.

        The file must be well formatted, otherwise a ScoreError is raised.

        >>> score = Score('fichiers/score.txt');
        >>> score.parseConfig();
        >>> score.get_identity()
        2
        >>> score2 = Score('fichiers/tests/scoreMissCroisillonCateg.txt');
        >>> score2.parseConfig()
        Traceback (most recent call last):
        ...
        ScoreError: Le croisillon ou l'espace sont manquants sur la première ligne : #[space]identity substitution indel --- Les catégories de score ne sont pas dans le bon ordre ou mal écrits: l'ordre correct est : identity ; substitution ; indel --- 
        """
        with open(self.get_fich(), "r") as fich:
            if self.isScore() != None:
                raise ScoreError(self.isScore()[1])
            valeurs = fich.readlines()[1].rstrip().split()
            self.set_identity(int(valeurs[0]))
            self.set_substitution(int(valeurs[1]))
            self.set_indel(int(valeurs[2]))


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)