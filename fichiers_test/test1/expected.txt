>seq_1 simple
TGTTACGG
>seq_2 simple
GGTTGACTA

Alignement score: 8

Alignment:

seq_1   2  GTT-AC  6  
           ||| ||     
seq_2   2  GTTGAC  7  

Significance of the score: 43.5% (200 sequences)

  score	     #
      0	     0:
      1	     0:
      2	     0:
      3	     0:
      4	     3:=
      5	    15:======
      6	    59:========================
      7	    36:==============
*     8	    53:=====================
      9	    11:====
     10	    18:=======
     11	     2:=
     12	     2:=
     13	     1:
