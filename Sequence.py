class FastaError(Exception):
    """
    Exception used by methods

    * ``__init__``
    * ``head``
    * ``tail``
    
    of class :class:`List`.
    """
    def __init__(self, msg):
        self.message = msg

class Sequence:
    """
    Class Sequence - Represent a sequence to align
    fich : The file where the sequence is read
    name : the name of the sequence ("" by default)
    sequence : the nucleotide sequence ("" by default)

    The file read and the sequence have to be well formatted : it is verified in the isFasta and parseFasta methods.
    """

    def __init__(self, fich, name="", sequence=""):

        self.__name = name
        self.__sequence = sequence
        self.__fich = fich
        self.__iupac = ['A', 'T', 'C', 'G', 'R', 'Y', 'S', 'W', 'K','M', 'B', 'D', 'H', 'V', 'N']

    def get_fich(self):
        return self.__fich

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_sequence(self):
        return self.__sequence

    def set_sequence(self, sequence):
        self.__sequence = sequence

    def isFasta(self):
        """
        Open a sequence file (fasta) and check if it is well formated.

        Parameters
        ----------

        Returns
        -------

        tuple
        None or False (well formated or not) and the text of the error

        The first line must contain a '>' followed by the name of the sequence (without spaces before the name)
        The second line must contain the sequence (ATCG) only

            >>> seq = Sequence('fichiers/seq1.fasta');
            >>> seq.isFasta()
            >>> seq = Sequence('fichiers/tests/scorePbScores.txt');
            >>> seq = Sequence('fichiers/tests/seqWrongFormat.fasta');
            >>> seq.isFasta()
            (False, 'Le chevron est manquant avant le nom de la séquence --- Un espace est présent dans la séquence.')
        """
        res = ''
        CHEVRON = "Le chevron est manquant avant le nom de la séquence --- "
        SPACE = "Un espace est présent dans la séquence."
        with open(self.get_fich(), "r") as fich:
            l = fich.readline()
            if l[0] != '>':
                res += CHEVRON
            l = fich.readline()
            for elt in l:
                if elt == ' ':
                    res += SPACE
        if res != '':
            return False, res
            

    def parseFasta(self):
        """
        Parse the sequence file (fasta) to complete the Sequence object.
        Check also if the sequence contains only nucleotides (ATCG).

        Parameters
        ----------

        Returns
        -------

        Nothing
        The attributes name and sequence are scpecified.

        The file must be well formatted, otherwise a FastaError is raised.
        Also, the sequence must contain only nucleotides (ATCG).

        >>> seq = Sequence('fichiers/seq1.fasta');
        >>> seq.parseFasta()
        >>> seq.get_name()
        'seq1'
        >>> seq2 = Sequence('fichiers/tests/seqWrongFormat.fasta');
        >>> seq2.parseFasta()
        Traceback (most recent call last):
        ...
        FastaError: Le chevron est manquant avant le nom de la séquence --- Un espace est présent dans la séquence.
        >>> seq3 = Sequence('fichiers/tests/seqWrongNucl.fasta');
        >>> seq3.parseFasta()
        Traceback (most recent call last):
        ...
        FastaError: Cette séquence contient des caractères qui ne sont ni des nucléotides ni des caractères du code IUPAC.
        """
        f = self.get_fich()
        if self.isFasta() != None:
            raise FastaError(self.isFasta()[1])
        with open(f, "r") as fich: 
            l = fich.readline() 
            ligne_entete = l.rstrip()
            seq = fich.readline().rstrip()
            for elt in seq:
                if elt not in self.__iupac:
                    raise FastaError("Cette séquence contient des caractères qui ne sont ni des nucléotides ni des caractères du code IUPAC.")
            name = ligne_entete[1:]
        self.set_name(name)
        self.set_sequence(seq)

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)