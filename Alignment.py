from Cost import *
from Score import *
from random import shuffle, sample

class Alignment:
    """
    Class Alignment - Represent a local Alignment of two sequences

    Parameters
    ----------
    seq1 : the first sequence to align
    object Sequence (parsed)
    seq2 : the second sequence to align
    object Sequence (parsed)
    cost : the system score used to align
    object Cost
    """

    def __init__(self, seq1, seq2, cost):

        self.__seq1 = seq1
        self.__seq2 = seq2
        self.__cost = cost

    def __get_seq1(self):
        return self.__seq1

    def __get_seq2(self):
        return self.__seq2

    def __get_cost(self):
        return self.__cost
    
    def __DPmatrix(self,U=''):
        """
        Calculate the matrix of the local alignement of 2 sequences

        Parameters
        ----------
        U : str
        The 1st sequence (if not specified, the sequence of the Alignment object will be used)

        Returns
        -------
        list of lists
        The matrix

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> alignement._Alignment__DPmatrix(seq1.get_sequence())
            [[(0, 'start'), (0, 'start'), (0, 'start'), (0, 'start'), (0, 'start'), (0, 'start'), (0, 'start')], [(0, 'start'), (0, 'H'), (0, 'H'), (5, 'D'), (1, 'G'), (0, 'H'), (0, 'H')], [(0, 'start'), (5, 'D'), (1, 'G'), (1, 'H'), (2, 'D'), (6, 'D'), (5, 'D')], [(0, 'start'), (1, 'H'), (10, 'D'), (6, 'G'), (2, 'G'), (2, 'H'), (3, 'D')], [(0, 'start'), (5, 'D'), (6, 'H'), (7, 'D'), (3, 'G'), (7, 'D'), (7, 'D')], [(0, 'start'), (1, 'H'), (2, 'H'), (11, 'D'), (7, 'G'), (3, 'H'), (4, 'D')]]
        """
        V = self.__get_seq2().get_sequence()
        lenU = len(U)
        lenV = len(V)

        #Initialization of the 1st lign and 1st column
        matrice = [[(0,'start')] for i in range(lenU + 1)]
        cost = self.__get_cost()
        for i in range(len(V)):
            matrice[0].append((0,'start'))

        #For each value of the matrix we test the 3 possibilities and we take the max
        for ligne in range(1,lenU+1):
            for colonne in range(1,lenV+1):
                haut = max(matrice[ligne-1][colonne][0] + int(self.__get_cost().get_sys_score().get_indel()),0),'H'
                gauche = max(matrice[ligne][colonne-1][0] + int(self.__get_cost().get_sys_score().get_indel()),0),'G'
                diago = max(matrice[ligne-1][colonne-1][0] + cost(U[ligne-1], V[colonne-1]),0),'D'
                ajout = max(haut,gauche,diago)
                matrice[ligne].append(ajout)
        return matrice
    
    
    def __DPmatrix2(self,U=''):
        """
        Calculates only the optimal score of local alignment of 2 sequences (not the alignment).
        Only 2 lists are used instead of a complete matrix.

        Parameters
        ----------
        U : str
        The 1st sequence (if not specified, the sequence of the Alignment object will be used)

        Returns
        -------
        int
        The optimal score of the alignment

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> alignement._Alignment__DPmatrix2(seq1.get_sequence())
            11
        """
        V = self.__get_seq2().get_sequence()
        scoreMax = 0
        cost = self.__get_cost()

        #Initialization of the 1st column and of the first value of the second column
        colonne1 = [0 for i in range(len(U) + 1)]
        colonne2 = [0]

        indexColonne = 0
        while indexColonne < len(V):
            for ligne in range (1,len(U)+1):
                haut = max(colonne2[ligne-1] + int(self.__get_cost().get_sys_score().get_indel()),0)
                gauche = max(colonne1[ligne] + int(self.__get_cost().get_sys_score().get_indel()),0)
                diago = max(colonne1[ligne-1] + cost(U[ligne-1], V[indexColonne]),0)
                ajout = max(haut,gauche,diago)
                colonne2.append(ajout)
                if ajout > scoreMax : 
                    scoreMax = ajout

            #We advance in the matrix with the 2 lists : the seond become the first and we initialized the next one
            colonne1 = colonne2
            colonne2 = [0]
            indexColonne += 1
        return scoreMax
    

    def __similarityScore(self, m):
        """
        Find the similarity score of the optimal alignment on a matrix

        Parameters
        ----------
        m : list of lists
        The matrix of the local alignment

        Returns
        -------
        tuple
        The similarity score and its postition in the matrix

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> m = alignement._Alignment__DPmatrix(seq1.get_sequence())
            >>> alignement._Alignment__similarityScore(m)
            (11, 5, 3)
            >>> alignement._Alignment__similarityScore(m)[0]
            11
        """
        maximum = 0
        imax = 0
        jmax = 0

        for i in range(len(m)):
            for j in range(len(m[0])):
                if m[i][j][0] > maximum :
                    maximum = m[i][j][0]
                    imax = i #we stock the position of the maximum to return it
                    jmax = j
        return maximum, imax, jmax

    def __alignement(self,m,a1='',a2=''):
        """
        Construct the alignment between 2 sequences, from the DP matrix

        Parameters
        ----------
        m : list of lists
        The matrix of the local alignment
        a1 : str
        The first sequence returned (sequence aligned)
        a2 : str
        The second sequence returned (sequence aligned)


        Returns
        -------
        tuple
        The 2 sequences aligned and the start position of the alignment on the matrix 

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> m = alignement._Alignment__DPmatrix(seq1.get_sequence())
            >>> m = alignement._Alignment__cutMatrix(m)
            >>> alignement._Alignment__alignement(m,seq1.get_sequence()[len(m)-2],seq2.get_sequence()[len(m[0])-2])
            ('ATAC', 'AT-C', 1, 0)
        """
        s1 = self.__get_seq1().get_sequence()
        s2 = self.__get_seq2().get_sequence()
        currVal = m[-1][-1][0] #we start from the bottom right corner of the matrix
        currI = len(m)-2 #actual value
        currJ = len(m[0])-2
        if currVal == 0: #if the value = 0 ; the alignment is over
            if a1[0] != '-':
                return a1[1:], a2[1:], currI+1, currJ
            elif a2[0] != '-':
                return a1[1:], a2[1:], currI, currJ+1
        elif currI == 0 or currJ == 0: #if we are on the first column or the first line, it's over
            return a1, a2, currI, currJ
        else:
            #we look from where the value comes from
            if m[currI][currJ][1] == 'D':
                m = m[:-1]
                for i in range(len(m)):
                    m[i] = m[i][:-1]
                return self.__alignement(m,s1[currI-1] + a1,s2[currJ-1] + a2)
            elif m[currI][currJ][1] == 'H':
                m = m[:-1]
                return self.__alignement(m,s1[currI-1] + a1,'-' + a2)
            else:
                for i in range(len(m)):
                    m[i] = m[i][:-1]
                return self.__alignement(m,'-' + a1,s2[currJ-1] + a2)

    def __prettyPrintAlign(self, align, opt):
        """
        Print the alignment found

        Parameters
        ----------
        align : tuple
        The result of the alignment method 
        opt : str
        The similarity score

        Returns
        -------
        None
        The alignment (sequences, start and end positions on the sequences, name of the sequences, similarity score) 
        """
        s1 = self.__get_seq1()
        s2 = self.__get_seq2()
        print('Séquence 1 : {} -> {}'.format(s1.get_name(), s1.get_sequence()))
        print('Séquence 2 : {} -> {}\n'.format(s2.get_name(), s2.get_sequence()))
        print("Score d'alignement optimal : {}\n".format(opt[0]))
        print("Alignement optimal : \n")
        print('{}  {}  {}  {}'.format(s1.get_name(),align[2]+1,align[0], opt[1]))
        print((len(s1.get_name())+5)*' ', end='')
        for i in range(len(align[0])):
            if align[0][i] == align[1][i]:
                print('|', end='')
            else:
                print(' ', end='')
        print('\n{}  {}  {}  {}\n\n'.format(s2.get_name(),align[3]+1, align[1], opt[2]))

    def __cutMatrix(self,m):
        """
        Prepare the matrix (cut it in order to have the optimal score on the bottom right) to do the alignment.

        Parameters
        ----------
        m : list of lists
        The matrix of the local alignment

        Returns
        -------
        list of lists
        The new matrix cutted in order to do the alignment.

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> m = alignement._Alignment__DPmatrix(seq1.get_sequence())
            >>> m = alignement._Alignment__cutMatrix(m)
            >>> m
            [[(0, 'start'), (0, 'start'), (0, 'start'), (0, 'start')], [(0, 'start'), (0, 'H'), (0, 'H'), (5, 'D')], [(0, 'start'), (5, 'D'), (1, 'G'), (1, 'H')], [(0, 'start'), (1, 'H'), (10, 'D'), (6, 'G')], [(0, 'start'), (5, 'D'), (6, 'H'), (7, 'D')], [(0, 'start'), (1, 'H'), (2, 'H'), (11, 'D')]]
        """
        m = m[:self.__similarityScore(m)[1]+1] #we keep the lines until the one containing the optimal score
        score = self.__similarityScore(m)[2]
        for i in range(len(m)): #then we keep the columns in the same way
            m[i] = m[i][:score+1]
        return m
        
    def __align(self):
        """
        Calculate and print the alignment for two sequences and a score system

        Parameters
        ----------

        Returns
        -------
        None
        The alignment (sequences, start and end positions on the sequences, name of the sequences, similarity score) 
        """
        seq1 = self.__get_seq1().get_sequence()
        seq2 = self.__get_seq2().get_sequence()
        matrice = self.__DPmatrix(seq1)
        opt = self.__similarityScore(matrice)
        matrice = self.__cutMatrix(matrice)
        res = self.__alignement(matrice,seq1[len(matrice)-2],seq2[len(matrice[0])-2])

        #printing
        self.__prettyPrintAlign(res, opt)


###########################################################################################################################################
        

    def __significance(self,s,n):
        """
        Calculate the significance of an alignment, comparing it to the score of the alignment of n random sequences to the second sequence

        Parameters
        ----------
        s : int
        The reference score 
        n : int
        The number of sequences generated from the first sequence

        Returns
        -------
        Couple
        The percentage of significance and a dictionnary containing the scores
        """
        alea = []
        scores = dict() #dict to stock the number of occurances of each score
        pct = 0

        #we generate n random sequences 
        for i in range(n):
            alea.append(sample(self.__get_seq1().get_sequence(), k = len(self.__get_seq1().get_sequence())))

        #for each random sequence, we calculate the optimal score and compare it to s
        for seq in alea:
            sim = self.__DPmatrix2("".join(seq))
            if sim > s :
                pct += 1
            if sim in scores.keys():
                scores[sim] += 1
            else:
                scores[sim] = 1
        return (pct * 100)/n,scores

    def __getMax(self,sig):
        """
        Return the max score we found by aligning the random sequences and the second sequence

        Parameters
        ----------
        sig : couple
        The result of the significance method

        Returns
        -------
        int
        The max score

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> sig = 25,{2: 3, 4: 1, 3: 1}
            >>> alignement._Alignment__getMax(sig)
            4
        """
        maxi = 0
        for k in sig[1].keys():
            if k > maxi:
                maxi = k
        return maxi

    def __findQuantiles(self,mini,maxi,modes):
        """
        Calculate the classes (or modes) between the lower score and the higher score

        Parameters
        ----------
        mini : int
        The lower score 
        maxi : int
        The higher score 
        modes : int
        The number of modes we want to create

        Returns
        -------
        List
        The list of the modes (integers)

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> alignement._Alignment__findQuantiles(5,15,10)
            [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
            >>> alignement._Alignment__findQuantiles(5,12,5)
            [6, 8, 9, 11, 12]
        """
        res = []
        etendue = maxi-mini
        for i in range(modes):
            #minimum score + difference between two classes of scores
            res.append(round(mini + ((i+1)/modes)*etendue))
        return res

    def __mostClose(self,val,quantiles):
        """
        Search the class to wich a value belongs to (closest value in the modes)

        Parameters
        ----------
        val : int
        The value we want to compare
        quantiles : list
        The list of quantiles (modes)

        Returns
        -------
        Int
        The mode 

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> quantiles = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
            >>> alignement._Alignment__mostClose(10, quantiles)
            10
            >>> quantiles = [6, 8, 9, 10, 13]
            >>> alignement._Alignment__mostClose(12, quantiles)
            13
        """
        dist,valeur = abs(val-quantiles[0]),quantiles[0]
        for elt in quantiles:
            if abs(val-elt) < dist:
                dist,valeur = abs(val-elt), elt
        return valeur


    def __findClasses(self,sig,modes):
        """
        Give the new distribution of scores with the modes

        Parameters
        ----------
        sig : tuple
        The initial distribution of scores
        modes : int
        The number of modes we want to create

        Returns
        -------
        List of tuples
        The values corresponding to their mode

            >>> seq1 = Sequence('fichiers/tests/seq1Doctests.fasta')
            >>> seq2 = Sequence('fichiers/tests/seq2Doctests.fasta')
            >>> seq1.parseFasta()
            >>> seq2.parseFasta()
            >>> cost = Cost(Score('fichiers/tests/score.txt',5,-3,-4), 'ATCGRYSWKMBDHVN');
            >>> alignement = Alignment(seq1,seq2,cost)
            >>> sig = 25,{2: 3, 17: 1, 8: 1}
            >>> alignement._Alignment__findClasses(sig, 5)
            [(5, 3), (8, 1), (17, 1)]
        """
        distrib = sig[1] #distibution of random sequences
        maxi = self.__getMax(sig) 
        mini = maxi

        #we search the minimum score of the distribution so we can calculate the quantiles
        for elt in distrib.keys():
            if elt < mini and elt != 0:
                mini = elt
        classes = self.__findQuantiles(mini,maxi,modes)
        newDistrib = dict()

        #then we search the quantile for each optimal score
        for elt in distrib.keys():
            mostClose = self.__mostClose(elt,classes)
            if mostClose not in newDistrib.keys():
                newDistrib[mostClose] = distrib[elt]
            else:
                newDistrib[mostClose] += distrib[elt]
        return sorted(newDistrib.items(), key=lambda t: t[0])

    def __displayDist(self,sig,s,n,modes):
        """
        Print the distribution of scores to show if the score is significant

        Parameters
        ----------
        sig : tuple
        The initial distribution of scores
        s : int
        The reference score 
        n : int
        The number of sequences generated from the first sequence
        modes : int
        The number of modes we want to create

        Returns
        -------
        None
        A pretty print of the distribution and the significance of the score s
        """
        classes = self.__findClasses(sig,modes)
        quantiles = [elt[0] for elt in classes]
        mostCloseS = self.__mostClose(s,quantiles)
        print("Significativité du score : {}% ({} séquences).\n".format(sig[0],n))
        print("Score          #\n")
        for elt in classes:
            if elt[0] != mostCloseS:
                print(" {:3}          {:3}:{}".format(elt[0],elt[1],"="*((elt[1]*100)//n)))
            else:
                print("*{:3}          {:3}:{}".format(elt[0],elt[1],"="*((elt[1]*100)//n)))

    def __call__(self, seq1, seq2, cost, modes, s):
        self.__align()
        simScore = self.__DPmatrix2(seq1.get_sequence())
        sig = self.__significance(simScore,s)
        self.__displayDist(sig,simScore,s,modes)


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)